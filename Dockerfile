FROM alpine
EXPOSE 53/udp
RUN apk add dnsmasq
CMD ["/usr/sbin/dnsmasq", "-d"]
